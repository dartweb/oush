#!/usr/bin/env node

/**
 * @file
 * @todo: description
 */

// Requirements.
const minimist = require('minimist');

const oush_auth = require('./includes/js/auth');
const oush_cli = require('./includes/js/cli');
const oush_config = require('./includes/js/config');

/**
 * Main program execution.
 */
async function run() {
  // Load the user configuration for connecting to OU Campus.
  var config = await oush_config.load();
  if (config) {
    // Authenticate into the OU Campus API and get back an access token.
    var token = await oush_auth.authenticate(config);
    // @todo, there should be an array of commands we check against to allow
    // running without authentication, such as help, config, login, etc.
    if (token) {
      // Perform the requested CLI command.
      var argv = minimist(process.argv.slice(2));
      await oush_cli.run(config, token, argv);
    }
  }
}

run();
