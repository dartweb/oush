/**
 * @file
 * @todo: description
 */

// Requirements.
const fs = require('fs-extra');
const inquirer = require('inquirer');
const ospath = require('ospath');
const yaml = require('js-yaml');

// Constants
const OUSH_CONFIG_FILEPATH = ospath.data() + '/oush/config.yml';

module.exports = {
  load,
  wizard
}

/**
 * Attempt to load and return valid oush configuration settings from a file.
 *
 * If for some reason an invalid configuration is returned, run the
 * configuration setup wizard until we have a valid one.
 */
async function load() {
  var config = {};
  try {
    // Ensure the correct file permissions are set on our config.
    fs.chmodSync(OUSH_CONFIG_FILEPATH, 0o600);
    // Attempt to load the config.
    config = yaml.safeLoad(fs.readFileSync(OUSH_CONFIG_FILEPATH, 'utf8'));
  }
  catch (error) {
    config = {};
  }

  while (!is_valid(config)) {
    var questions = [{
      type: 'confirm',
      name: 'run_wizard',
      message: 'Unable to detect a valid oush configuration. Would you like to run the setup wizard?',
    }];
    answers = await inquirer.prompt(questions)
    if (answers['run_wizard']) {
      config = await wizard();
    }
    else {
      console.log('Exiting...');
      process.exit(1);
    }
  }

  return config;
}

/**
 * For a passed-in oush configuration array, determine if it is valid or not.
 */
function is_valid(config) {
  if (
    (config && typeof config === "object") &&
    config['username'] &&
    config['password'] &&
    config['skin'] &&
    config['account'] &&
    config['api_url']
  ) {
    return true;
  }

  return false;
}

/**
 * Configure oush by asking the user for the relevant information needed
 * to connect to their OU Campus instance via the REST API. Take the given
 * answers and then write them to the oush configuration file.
 *
 * https://a.cms.omniupdate.com/10/#outc19/workshop67/gallena
 */
async function wizard() {
  console.log("Running oush configuration wizard...\n");
  var config = {};

  const questions = [
    {
      type: 'input',
      name: 'username',
      message: 'Enter your OU Campus username:',
    },
    {
      type: 'password',
      name: 'password',
      message: 'Enter your OU Campus password:',
    },
    {
      type: 'input',
      name: 'dashboard_url',
      message: 'Enter the URL of your OU Campus dashboard page:',
    },
  ];
  answers = await inquirer.prompt(questions);

  // Use this regular expression to extract the API URL, skin, and account name
  // from the user's OU Campus dashboard URL.
  var regex = /^https?:\/\/([^\/]+)\/\d+\/#([^\/]+)\/([^\/]+)\/[^\/]+\/?.*$/i;
  var matches = answers['dashboard_url'].match(regex);

  var api_url = matches[1];
  var skin = matches[2];
  var account = matches[3];

  if (api_url && skin && account) {
    config = {
      username: answers['username'],
      password: answers['password'],
      skin: skin,
      account: account,
      api_url: api_url,
    };

    await fs.ensureFile(OUSH_CONFIG_FILEPATH, error => {
      if (error) {
        console.log('fs.ensureFile error: ' + error);
      }
    });

    await fs.writeFile(OUSH_CONFIG_FILEPATH, yaml.safeDump(config), 'utf8', error => {
      if (error) {
        console.log('fs.writeFile error: ' + error);
      }
    });
  }
  else {
    console.log('Error: Unable to parse OU Campus dashboard URL. Please re-check your URL and try again.');
  }

  return config;
}
