/**
 * @file
 * @todo: description
 */

// Requirements.
const request = require('request-promise');

// Exports.
module.exports = {
  authenticate,
}

/**
 * Authenticate into the OU Campus REST API, and if successful, return a valid
 * access token.
 */
async function authenticate(config) {
  var token = null;

  var params = {
    headers: {'content-type' : 'application/x-www-form-urlencoded'},
    url: "https://" + config['api_url'] + "/authentication/login",
    body: "skin=" + config['skin'] + "&username=" + config['username'] + "&password=" + config['password'] + "&account=" + config['account']
  };

  await request.post(params, function(error, response, body) {
    var obj = JSON.parse(body);
    token = obj['gadget_token'];

    if (!token) {
      console.log("Error: Unable to authenticate. " + body);
    }
  });

  return token;
}
