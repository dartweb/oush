/**
 * @file
 * @todo: description
 */

// Requirements.
const request = require('request-promise');
const fs = require('fs-extra');
const yaml = require('js-yaml');
const table = require('table');

const ou_api = require('./ou_api');

// Exports.
module.exports = {
  run,
}

// Load all available oush commands and options.
var cli_commands = {};
var cli_global_options = {};
try {
  cli_commands = yaml.safeLoad(fs.readFileSync('./includes/yml/commands.yml', 'utf8'));
  cli_global_options = yaml.safeLoad(fs.readFileSync('./includes/yml/global_options.yml', 'utf8'));
}
catch (error) {
  var cli_commands = {};
  var cli_global_options = {};
}

/**
 * Run an OUSH CLI command.
 */
async function run(config, token, argv) {
  var command = argv["_"].shift();

  var arguments = argv["_"];
  delete argv._;

  var options = argv;

  // Verify that the command exists and all required arguments and options have
  // been passed in and are valid.
  var output = '';
  if (await is_valid(command, arguments, options)) {
    var command_parts = command.split(":");
    var command_obj = cli_commands;
    command_parts.forEach(function(element) {
      if (element in command_obj) {
        command_obj = command_obj[element];
      }
    });

    // Convert the arguments from an array to an indexed object.
    arguments = await convert_arguments_to_object(command_obj, arguments);

    // If the command has a method and endpoint, we simply execute it against
    // the API and return the results.
    if ("method" in command_obj && "endpoint" in command_obj) {
      output = await ou_api.execute(command_obj, arguments, options, config, token);
    }
    else {
      // Otherwise, we run the javascript callback for this command instead.
      var func_name = "oush_" + command.replace(/:/gi, "_").replace(/-/gi, "_");
      // @todo: eval is considered bad form. Is there a way to dynamically call
      // a function without it? Investigate...
      var output = await eval(func_name + '(command_obj, arguments, options, config, token);');
    }

    console.log('command output: ' + JSON.stringify(output, null, 2));
  }
}

/**
 * Verify that a CLI command and the associated passed-in arguments and options
 * are valid.
 *
 * @param string command
 *   An OUSH command.
 *
 * @param array argv
 *   An array of command line arguments and options.
 */
async function is_valid(command, cmd_arguments, cmd_options) {
  var fail = false;

  // If they didn't enter a command, output a help message and exit.
  if (!command) {
    console.log('@todo: no command entered.');
    return false;
  }

  // Verify the command exists.
  var command_parts = command.split(":");
  var command_obj = cli_commands;
  command_parts.forEach(function(element) {
    if (element in command_obj) {
      command_obj = command_obj[element];
    }
    else {
      console.log("Error: The function '" + command + "' is not defined.");
      fail = true;
      return false;
    }
  });
  if (fail) {
    return false;
  }

  // Next, verify the command is implemented.
  var is_api_func = false;
  if ("method" in command_obj && "endpoint" in command_obj) {
    is_api_func = true;
  }

  var is_callback_func = false;
  var func_name = "oush_" + command.replace(/:/gi, "_").replace(/-/gi, "_");
  console.log('func_name: ' + func_name);
  // @todo: eval is considered bad form. Is there a way to dynamically check
  // if a function exists without it? Investigate...
  if (eval("typeof " + func_name) === 'function') {
    is_callback_func = true;
  }

  if (!is_api_func && !is_callback_func) {
    console.log("Error: The function '" + command + "' has not been implemented.");
    return false;
  }

  // Finally, verify the arguments and options are valid.
  var ref_arguments = command_obj["arguments"];
  var ref_options = command_obj["options"];

  // Did the user submit too many arguments?
  if (cmd_arguments.length > Object.keys(ref_arguments).length) {
    console.log("Error: too many arguments.");
    return false;
  }

  // Are we missing required argument(s), or using disallowed values?
  var i = 0;
  if (Object.keys(ref_arguments).length > 0) {
    Object.keys(ref_arguments).forEach(function(index) {
      let ref_item = ref_arguments[index];
      let required = false;
      if ("required" in ref_item && ref_item["required"] === true) {
        required = true;
        if (typeof cmd_arguments[i] === "undefined") {
          console.log("Error: missing required argument '" + index + "'.");
          fail = true;
          return false;
        }
      }
      if ("allowed_values" in ref_item) {
        if (
          (required && ref_item["allowed_values"].indexOf(cmd_arguments[i]) === -1) ||
          (!required && (typeof cmd_arguments[i] !== "undefined") && ref_item["allowed_values"].indexOf(cmd_arguments[i]) === -1)
        ) {
          console.log("Error: invalid value for argument '" + index + "'.");
          fail = true;
          return false;
        }
      }
      i++;
    });
    if (fail) {
      return false;
    }
  }

  // Verify the passed-in options are valid.
  // First, convert flags into their full commands.
  cmd_options = await convert_flags_to_options(command_obj, cmd_options);
  if (Object.keys(cmd_options).length > 0) {
    Object.keys(cmd_options).forEach(function(option) {
      // Does the option exist?
      if (typeof ref_options[option] == "undefined") {
        console.log("Error: unrecognized option '" + option + "'.");
        fail = true
      }
      // Is the given value allowed?
      else if (
        ("allowed_values" in ref_options[option]) &&
        (ref_options[option]["allowed_values"].indexOf(cmd_options[option]) === -1)
      ) {
        console.log("Error: invalid value for option '" + option + "'.");
        fail = true;
      }
    });
    if (fail) {
      return false;
    }
  }

  return true;
}

/**
 * Given an array of CLI options, make sure that any flags are expanded out
 * to their full command. Error out if we receive unknown flags.
 *
 * @todo: support global flags
 *
 * @todo: should it error out if a person enters both the option and its flag?
 *        as it is, the flag is just overwriting the option.
 */
function convert_flags_to_options(command_obj, cmd_options) {
  ref_options = {};
  if (typeof command_obj["options"] != "undefined") {
    ref_options = command_obj["options"];
  }

  if (Object.keys(cmd_options).length > 0) {
    Object.keys(cmd_options).forEach(function(flag) {
      if (flag.length === 1) {
        var success = false;

        if (Object.keys(ref_options).length > 0) {
          Object.keys(ref_options).forEach(function(option) {
            if (typeof ref_options[option]["flag"] != "undefined") {
              if (ref_options[option]["flag"] === flag) {
                success = true;
                cmd_options[option] = cmd_options[flag];
                delete cmd_options[flag];
              }
            }
          });
        }

        if (!success) {
          console.log("Error: unknown option flag '" + flag + "'.");
          process.exit(1);
        }
      }
    });
  }

  return cmd_options;
}

/**
 * @todo: description
 */
function convert_arguments_to_object(command_obj, cmd_arguments) {
  ref_arguments = {};
  if (typeof command_obj["arguments"] != "undefined") {
    ref_arguments = command_obj["arguments"];
  }

  args_obj = {};
  var i = 0;
  if (Object.keys(ref_arguments).length > 0) {
    Object.keys(ref_arguments).forEach(function(index) {
      if (typeof cmd_arguments[i] == "undefined") {
        args_obj[index] = "";
      }
      else {
        args_obj[index] = cmd_arguments[i];
      }
      i++;
    });
  }

  return args_obj;
}

/**
 * Custom-implemented oush commands. (I.E. Commands that don't directly
 * correlate to an OU Campus API endpoint response.)
 */

/**
 * Implements the command `oush help`.
 *
 * By itself, this command lists all available oush commands. If a command name
 * is passed in as an argument, then it lists helpful information about the
 * specific command.
 *
 * @param string command
 *   If provided, list specific helpful details about the given oush command.
 */
async function oush_help(command_obj, arguments, options, config, token) {
  let table_width = process.stdout.columns - 6;
  if (table_width == undefined) {
    table_width = 80;
  }

  if (arguments["command"].length > 0) {
    console.log('haaaalp! (specific command)');
  }
  else {
    // Create a full list of all available commands.
    config = {
      columns: {
        0: {
          width: parseInt(table_width * .2)
        },
        1: {
          width: parseInt(table_width * .8),
          wrapWord: true
        }
      }
    }

    // Output all core commands.
    console.log ("core oush commands:");
    let data = []
    Object.keys(cli_commands).forEach(function(command) {
      if (typeof cli_commands[command]["description"] != "undefined") {
        data.push([command, cli_commands[command]["description"]]);
      }
    });
    if (data.length > 0) {
      let output = table.table(data, config);
      console.log(output);
    }
    else {
      console.log("Warning: No commands in this group have been implemented yet.");
    }

    // Output all grouped commands.
    Object.keys(cli_commands).forEach(function(group) {
      if (typeof cli_commands[group]["description"] == "undefined") {
        let data = [];
        Object.keys(cli_commands[group]).forEach(function(command) {
          if (typeof cli_commands[group][command]["description"] == "undefined") {
            Object.keys(cli_commands[group][command]).forEach(function(subcommand) {
              data.push([group + ":" + command + ":" + subcommand, cli_commands[group][command][subcommand]["description"]]);
            });
          }
          else {
            data.push([group + ":" + command, cli_commands[group][command]["description"]]);
          }
        });
        if (data.length > 0) {
          let output = table.table(data, config);
          console.log(group + " commands:");
          console.log(output);
        }
      }
    });
  }
}

/**
 * Implements the command `oush config`.
 *
 * Re-run the oush configuration wizard.
 *
 * @param string command
 *   If provided, list specific helpful details about the given oush command.
 */
async function oush_config(command_obj, arguments, options, config, token) {
  let ouconfig = require('./config');
  await ouconfig.wizard();
}
