/**
 * @file
 * @todo: description
 */

// Requirements.
const request = require('request-promise');
const querystring = require('querystring');

// Exports.
module.exports = {
  execute,
}

/**
 * Execute a command against the OmniUpdate REST API.
 */
async function execute(command_obj, arguments, options, config, token) {
  var output = null;

  var params = {
    authorization_token: token
  };
  Object.assign(params, arguments, options);
  //console.log('params obj: ' + params);
  params = querystring.stringify(params);
  //console.log("params, stringified: " + params);

  try {
    if (command_obj["method"] === "GET") {
      //console.log('we are doing a GET');
      await request.get(
        "https://" + config["api_url"] + command_obj["endpoint"] + "?" + params,
        function(error, response, body) {
          output = JSON.parse(body);
        }
      );
    }
    else if (command_obj["method"] === "POST") {
      //console.log('we are doing a POST');
      await request.post({
        headers: {'content-type' : 'application/x-www-form-urlencoded'},
        url: "https://" + config["api_url"] + command_obj["endpoint"],
        body: params,
      },
      function(error, response, body) {
        output = JSON.parse(body);
      });
    }
  }
  catch (error) {
    console.log("Error: " + JSON.stringify(error, null, 2));
  }

  return output;
}


















/**
 * Output a list of users in the system.
 *//*
function oush_user_list(argv) {
  var request = require('request');
  request.get(OU_API_URL + "/users/list?authorization_token=" + token, function(error, response, body) {
    var obj = JSON.parse(body);
    console.log(obj);
  });
}

/**
 * Create a new user in the system.
 *//*
 function oush_user_create(argv) {
  var request = require('request');
  request.post({
    headers: {'content-type' : 'application/x-www-form-urlencoded'},
    url: OU_API_URL + "/users/new",
    body: "username=" + username + "&password=" + password + "&authorization_token=" + token,
  },
  function(error, response, body) {
    var obj = JSON.parse(body);
    console.log(obj);
  });
 }

/**
 * Check out a given page.
 *//*
function oush_checkout_page(argv) {
  var request = require('request');
  request.post({
    headers: {'content-type' : 'application/x-www-form-urlencoded'},
    url: OU_API_URL + "/files/checkout",
    body: "site=" + site + "&path=" + path + "&authorization_token=" + token,
  },
  function(error, response, body) {
    var obj = JSON.parse(body);
    console.log(obj);
  });
}

/**
 * Check in a given page.
 *//*
function oush_page_checkin(argv) {
  var request = require('request');
  request.post({
    headers: {'content-type' : 'application/x-www-form-urlencoded'},
    url: OU_API_URL + "/files/checkin",
    body: "site=" + site + "&path=" + path + "&authorization_token=" + token,
  },
  function(error, response, body) {
    var obj = JSON.parse(body);
    console.log(obj);
  });
}

/**
 * Publish an entire site.
 *//*
function oush_site_publish(argv) {
  var request = require('request');
  request.post({
    headers: {'content-type' : 'application/x-www-form-urlencoded'},
    url: OU_API_URL + "/sites/publish",
    body: "site=" + site + "&authorization_token=" + token,
  },
  function(error, response, body) {
    var obj = JSON.parse(body);
    console.log(obj);
  });
}

/**
 * List all available oush commands.
 *//*
function oush_help(config, token, argv) {
  console.log("");
  console.log("OmniUpdate Shell - Version 0.00001");
  console.log("Available Commands:");
  console.log(" - help (Show this list of commands.)");
  console.log(" - user list (List all users in the system.)");
  console.log(" - user create <username> <password> (Create a new user.)");
  console.log(" - page checkout <sitename> <path> (Checkout a page.)");
  console.log(" - page checkin <sitename> <path> (Check in a page.)");
  console.log(" - site publish <sitename> (Publish an entire site.)");
  console.log("");
}*/
